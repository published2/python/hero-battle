import os

os.system("")


class HealthBar:
    symbol_remaining: str = "█"
    symbol_lost: str = "_"
    barrier: str = "|"
    colors: dict = {"red": "\033[91m",
                    "purple": "\33[95m",
                    "blue": "\33[34m",
                    "green": "\033[92m",
                    "yellow": "\33[93m",
                    "default": "\033[0m"
                    }

    def __init__(self,
                 entity_obj,
                 lenght: int = 20,
                 is_colored: bool = True,
                 color: str = "") -> None:
        self.entity_obj = entity_obj
        self.lenght = lenght
        self.current_value = entity_obj.health
        self.max_value = entity_obj.health_max
        self.is_colored = is_colored
        self.color = self.colors.get(color) or self.colors["default"]

    def update(self) -> None:
        self.current_value = self.entity_obj.health

    def draw(self) -> None:
        remining_bars = round(self.current_value / self.max_value * self.lenght)
        lost_bars = self.lenght - remining_bars
        print(f"{self.entity_obj.name}'s HEALTH: {self.entity_obj.health}/{self.entity_obj.health_max}")
        print(f"{self.barrier}"
              f"{self.color if self.is_colored else ''}"
              f"{remining_bars * self.symbol_remaining}"
              f"{lost_bars * self.symbol_lost}"
              f"{self.colors['default'] if self.is_colored else ''}"
              f"{self.barrier}")
